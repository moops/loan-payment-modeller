
is_leap_year = lambda year : year % 4 == 0 and year % 100 != 0

def days_in_month(year, month):
	if month in ["january", "march", "may", "july", "august", "october", "december"]:
		return 31
	elif month in ["april","june","september","november"]:
		return 30
	else:
		return 29 if is_leap_year(year) else 28

