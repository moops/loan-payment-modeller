
from operator import attrgetter;
from decimal import *
from calendar import *
from accounting import *

getcontext().prec = 6

class loan_datum():
	def __init__(self, line, function):
		#split = line.split(",")
		#id, payment, principal, rate = split[0],split[1],split[2],split[3],
		
		id, payment, principal, rate = split = line.split(",")
		self.id = id
		self.rate = Decimal(rate)
		self.daily_rate = Decimal( (1 + Decimal(self.rate)) ** Decimal(1 / 365) - 1 )
		self.principal = Decimal(principal)
		self.payment = Decimal(payment)
		self.function = function

	def __repr__(self):
		return "id: {0}, principal: {1}, rate: {2}, payment: {3}\n".format(self.id, self.principal, self.rate, self.payment)

	def make_scheduled_payment(self, payment_balance, year, month):
		interest = self.monthly_interest(year, month)
		balance_at_payment = self.principal
		payment = self.payment if self.payment <= balance_at_payment else balance_at_payment
		new_payment_balance = payment_balance - payment
		self.principal = balance_at_payment - payment + interest
		return new_payment_balance

	def make_extra_payment(self, payment_balance):
		payment = payment_balance if self.principal >= payment_balance else self.principal
		new_payment_balance = payment_balance - payment
		self.principal -= payment 
		return new_payment_balance

	def monthly_interest(self, year, month):  
		days = days_in_month(year, month)
		new_balance = future_value(self.principal,days,self.daily_rate)
		return new_balance - self.principal

	def daily_interest(self):
		return self.principal * self.daily_rate



