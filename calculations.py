from math import log, expm1

class annualized_rate():
	def __init__(self,apr):
		self.apr = apr

	def to_monthly_rate(self):
		return self.to_time_frame(12)
		
	def to_daily_rate(self):
		return self.to_time_frame(365)

	def present_value(self,future_value,number_of_terms):
		tau = (1 + self.apr) ** number_of_terms
		return future_value / tau

	def future_value(self,present_value, number_of_terms):
		tau = (1 + self.apr) ** number_of_terms
		return present_value * tau

	def to_time_frame(self,time_factor):
		return expm1(log(1 + self.apr) / time_factor)
	
	def daily_accrual(self, principal):
		return self.to_daily_rate() * principal

	def desired_accrual(self,principal,desired):
		target_principal = desired / self.to_daily_rate()	


loans = [
	(0.03860, 3955.09),
	(0.03860, 4295.40),
	(0.05600, 2489.33),
	(0.06800, 4797.62 - 650),
	(0.03400, 850.75),
	(0.03400, 2939.66),
	(0.06800, 3733.47),
	(0.04660, 3924.94),
	(0.04660, 5471.09),
	(0.04290, 2129.52),
	(0.04290, 7046.27 - 650)
]

enumerated_loans = map(lambda l: (l[0] + 1,l[1]), enumerate(loans))

def accrual(d):
	return annualized_rate(d[1][0]).daily_accrual(d[1][1])
for datum in sorted(enumerated_loans, key=accrual,reverse=True):
	number, loan = datum
	apr, principal = loan
	print("loan {0}:\n\tapr:\t{1}\n\tprincipal:\t{2}\n\tdaily_accrual:\t{3}".format(
		number,
		apr,
		principal,
		annualized_rate(apr).daily_accrual(principal)))


