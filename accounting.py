
def future_value(principal, periods, rate):
	return principal * (1 + rate) ** periods

def present_value(future_principal, periods, rate):
	return future_principal / ((1 + rate) ** periods) 

