#!/usr/bin/python3

from operator import attrgetter;
from decimal import *
from loan_datum import *
from operator import add
from functools import reduce

calculate_daily_interest = lambda principal, rate: (Decimal(principal) * Decimal(rate)) / Decimal(365.26)

def read_loan_data(function):
	with open('loan-data.csv') as file:
		return [loan_datum(line, function) for line in file]

total_principal = lambda list : sum(d.principal for d in list)
total_payment = lambda list : sum(d.payment for d in list)

def get_date_generator(month, year):
	month_lookup = ["january","february","march","april","may","june","july","august","september","october","november","december"]
	def get_date(month, year):
		next_month_lookup ={ pair[0]:pair[1] for pair in zip(month_lookup,month_lookup[1:] + month_lookup[0:1])}
		generated_month = month
		generated_year = year
		while True:
			yield generated_month, generated_year
			if generated_month == month_lookup[-1]:
				generated_year = generated_year + 1
			generated_month = next_month_lookup[generated_month]
	if month not in month_lookup:
		raise "That is not a correct month"
	return get_date(month,year)

def make_scheduled_payment(data, payment, year, month,select = max):
	balance = payment
	print(25 * '*' + f"{month} {year}")
	print(f"before scheduled payments, payment balance is {balance}")
	for datum in data:
		balance = datum.make_scheduled_payment(balance, year, month)
	while balance > Decimal(0.01) and total_principal(data) > Decimal(0.00): 
		selected_datum = select(data, key=lambda datum : datum.function(datum))
		text = f"for datam {selected_datum.id}, payment balance is {balance} and principal is {selected_datum.principal}\n"
		balance = selected_datum.make_extra_payment(balance)
		text += f"\tafter overpayment for loan {selected_datum.id}, balance is {Decimal(balance)}\n"
		text += f"\t\t and remaining balance for loan {selected_datum.id} is {selected_datum.principal}\n"
		text += f"\ttotal_principal = {total_principal(data)}\n"
	#	ptext = f"\tTotal principal is {total_principal(data)}"
	#	print(ptext) 
		if selected_datum.principal < Decimal(0.01):
			data.remove(selected_datum)


def gdg(month, year):
	month_lookup = ["january","february","march","april","may","june","july","august","september","october","november","december"]
	def gd(month, year):
		m = month_lookup.index(month)
		months = month_lookup[m:]
		while True:
			for the_month in months:
				yield the_month, year
			year=year+1
			months = month_lookup
	return gd(month, year)

def get_payment(month, year):
	payment = Decimal(649.99)
	overpayment = Decimal(100)
	for m,y in gdg(month, year):
		if m =="march":
			overpayment = overpayment  + Decimal(80)
		#	yield (overpayment * Decimal(12)) + payment
		if m == "october" and y == 2022:
			overpayment = overpayment + Decimal(30)
		yield payment + overpayment

def model_loans(pass_in_func = None ,max_or_min = max):
	func = pass_in_func 
	if pass_in_func == None:
		func = lambda d: calculate_daily_interest(d.principal, d.rate) 
	data = read_loan_data(func)
	dates = gdg("january", 2022)
	month, year = dates.__next__()
	payments = get_payment("january", year)
	while total_principal(data) > 0:
		payment = payments.__next__()
		make_scheduled_payment(data, payment,year, month, max_or_min)
		data = sorted([loan for loan in data if loan.principal > 0 ],key= func )
		month, year = dates.__next__()
	return (year,month)

get_daily_interest = lambda d: calculate_daily_interest(d.principal, d.rate)
get_principle = lambda d: d.principal
get_loan_payment = lambda d: d.payment
get_rate = lambda d: d.rate
		
#print(get_daily_interest(loan_datum('1, 56.43, 4820.04, 0.03860', get_daily_interest)))
#print(loan_datum('1, 56.43, 4820.04, 0.03860', get_daily_interest).function(loan_datum('1, 56.43, 4820.04, 0.03860', get_daily_interest)))
#model_loans(get_daily_interest,max_or_min = max)
#model_loans(get_rate)
#model_loans(get_principle)
#model_loans()

for name, method,selector in [ 
	("max-payment",get_loan_payment, max),
	("min-payment",get_loan_payment, min),
	("max-principle", get_principle, max),
	("min-principle", get_principle, min),
	("max-daily-interest",get_daily_interest, max),
	("min-daily-interest",get_daily_interest, min),
	("min-interest-rate", get_rate, min),
	("max-interest-rate", get_rate, max)
]:
	finish_year,finish_month = model_loans(method)
	print(f"For method {name}, finished at {finish_month} {finish_year}")
	print(150 * '#')

#sort_key_function = lambda d: d.daily_interest()
#data = read_loan_data(get_daily_interest)
#for d in sorted(data,key=sort_key_function,reverse=True):
#	print("id:{0}\nprinciple:{1}\ndaily interest:{2}\nmonthyly interest:{3},{4}\npayment size:{5}\n".format(d.id,d.principal,d.daily_interest(),d.daily_interest() * 30,d.daily_interest() * 31,d.payment))
#interests = map(lambda d:d.daily_interest(),data)
#total_daily_interest = reduce(add,interests)
#print("Total daily interest: {}".format(total_daily_interest))
#print("Total monthly interest: {}".format(total_daily_interest * 30))

# how to find out how many payments left at current interest/payment plan
#    It's a series.  Current principal + total monthly interest for the month - payment.  Increment month, increment counter
